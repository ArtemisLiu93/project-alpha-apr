from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import Create_Project_Form


# Create your views here.
@login_required
def project_lists(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    projects_2 = get_object_or_404(Project, id=id)
    context = {
        "project_2": projects_2,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    form = Create_Project_Form()
    if request.method == "POST":
        form = Create_Project_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    context = {"form": form}
    return render(request, "projects/create.html", context)
