from django.contrib.auth.decorators import login_required
from .forms import Create_Task_Form
from django.shortcuts import redirect, render
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = Create_Task_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = Create_Task_Form()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def list_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)
